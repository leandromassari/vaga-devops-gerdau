# vaga-devops-gerdau

Teste para vaga de DevOps Gerdau
1- Dockerfile:

Crie um ou mais Dockerfile de acordo com nossa stack atual: CentOS 7 Java 8 ou 9

OBS: Caso queira incluir mais algumas instalações ou configurações fique a vontade! ;)

2- Infra as code:

Desenvolva um código, utilizando terraform, para criar uma parte da infra que utilizamos na Gerdau. A idéia é criar um cluster ECS, pelo menos um service e uma task. Se achar necessário pode incluir outros elementos de infraestrutura como VPC, subnets, Auto Scaling Group e launch configuration.

Links de suporte:

https://www.terraform.io/docs/providers/aws/r/ecs_service.html

https://www.terraform.io/docs/providers/aws/r/ecs_cluster.html

https://www.terraform.io/docs/providers/aws/r/ecr_repository.html

https://www.terraform.io/docs/providers/aws/r/ecs_task_definition.html

3- Stack de ferramentas:

Desenhe/descreva um ciclo de CI/CD desde o backlog até o deploy da aplicação, incluindo ferramentas de mercado de infra as code, build, continuous integration, deploy, gestão de dependências e operações (monitoração e logs). Justifique a escolha das ferramentas que achar mais relevantes.

#Processo de submissão

Fazer um fork desse repositório e nos mandar um pull request. Email:juliana.goncalves1@gerdau.com.br, eustaquio.pereira@gerdau.com.br